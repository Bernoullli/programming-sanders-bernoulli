# -*- coding: utf-8 -*-
###Collatz problem
### This function compute the value of the fucntion c of collatz for a given n
##it also count the number of iteration with the variable r
def coll(n,r=0): 
    if n==1:#the initial value
        return 1,r+1
    elif n % 2 ==0:# if n is even
        return coll(n/2,r+1)
    else:
        return coll(3*n+1,r+1)
print "Some value of the function C of Collatz with number of iteration"
print "n | iteration"
for i in range(1,6):       
    print coll(i)
##### fibonacci sequence #######
#### This algorithm replicate the algorithme given in the course
def fib(n):
    x,y=0,1
    for i in range(n):
        x,y = y,x+y #we store the last value in the first component of the 
    return x
print "Some value of the fibonnacci sequence using the algorithm in the course"#
for i in range(50):
    print "fib","(",i,")","=",fib(i)
print "fib","(",100,")","=",fib(100)

##### fibonacci using matrix #####
import numpy as np
def fib1(n):
    fi=np.transpose(np.matrix([0,1])) #the vector that contain two consecutive value of the fibonnacci sequence
    A=np.matrix([[0,1],[1,1]]) #The Matrix A of the linear equation
    for i in range(n):
        fi=A * fi #Multiply the vector and the matrix n times
    return int(fi[0]) #The value of the fib1(n) transformed into an integer


print "Comparison between the algorithm in the course and the algorithm using matrix"
print "     |Course | Matrix "
for i in range(50):
    print i ,":","  ",fib(i),"         ",fib1(i)

### factorial ######
def fact(n):
    x,y=1,1
    for i in range(1,n+1):
        x,y = y,(i*y)
    return y
print "The value of factorial 100",fact(100)